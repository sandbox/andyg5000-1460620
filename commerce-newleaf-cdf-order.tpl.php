<?php
/**
 * @file
 * Default txt Schema template for New Leaf Order.
 * 
 * Available variables:
 * - order: the commerce order object.
 * - order_products: the orders products
 * - addresses: the orders addresses
 * - shipping type: shipping type formated for new leaf
 * - newleaf: array of user/pass for new leaf auth
 */
?>
CDFORD
BT*<?php print $newleaf['buyer_id'];?>*<?php print $newleaf['buyer_password'];?>*
ST*<?php print $addresses['shipping_address']['name_line']; ?>*<?php print $addresses['shipping_address']['thoroughfare']; ?>*<?php print $addresses['shipping_address']['premise']; ?>**<?php print $addresses['shipping_address']['locality']; ?>*<?php print $addresses['shipping_address']['administrative_area']; ?>*<?php print $addresses['shipping_address']['postal_code']; ?>*<?php print $addresses['shipping_address']['country']; ?> 
STI*R*<?php print $shipping_type; ?> 
CON*<?php print $order->order_number; ?> 
FTC*Y 
SP*Y 
<?php foreach ($order_products as $product): ?>
ID*<?php print $product['count']; ?>*<?php print $product['sku']; ?>*<?php print $product['quantity'] ?>*<?php print $product['unit_price'] ?>*<?php print $product['unit_price'] ?> 
<?php endforeach; ?>
NOI*<?php print sizeof($order_products); ?> 
ENDORD 